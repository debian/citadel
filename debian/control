Source: citadel
Section: mail
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: bison,
               debhelper-compat (= 12),
               libc-ares-dev (>= 1.7.2),
               libcitadel-dev (>= 917),
               libcurl4-openssl-dev (>> 7.25),
               libdb-dev,
               libev-dev (>= 4.0),
               libexpat1-dev,
               libical-dev,
               libldap2-dev,
               libncurses5-dev,
               libpam0g-dev,
               libsieve2-dev,
               libssl-dev (>= 1.1.0),
               po-debconf,
               zlib1g-dev
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: http://www.citadel.org/
Vcs-Browser: https://salsa.debian.org/debian/citadel
Vcs-Git: https://salsa.debian.org/debian/citadel.git

Package: citadel-server
Architecture: any
Depends: adduser,
         lsb-base (>= 3.0-6),
         openssl,
         patch,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: db4.6-util, shared-mime-info
Replaces: citadel-common, citadel-mta, mail-transport-agent
Conflicts: citadel-common,
           citadel-mta,
           imap-server,
           mail-transport-agent,
           pop3-server
Provides: citadel-mta, imap-server, mail-transport-agent, pop3-server
Description: complete and feature-rich groupware server
 Citadel is a complete and feature-rich open source groupware platform.
  * Email, calendaring/scheduling, address books
  * Bulletin boards, mailing list server, instant messaging
  * Multiple domain support
  * An intuitive, attractive AJAX-style web interface
 .
 The Citadel system is extremely versatile. It provides numerous front ends to
 present to users, such as a text-based interface, an AJAX-style web interface,
 and many popular PIM clients using SMTP/POP/IMAP. All of these can be used
 simultaneously.
 .
 It's also extremely scalable. Not only can a well-equipped Citadel server
 support a large number of concurrent users, but you can also build
 a distributed network of Citadel nodes that share rooms and their content.

Package: citadel-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: complete and feature-rich groupware server (documentation)
 This package contains documentation for Citadel, a complete and feature-rich
 open source groupware platform.
 .
 See the 'citadel-server' package for more information.
