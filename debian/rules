#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# This has to be exported to make some magic below work.
include /usr/share/dpkg/architecture.mk

export DH_OPTIONS

DEB_DESTDIR = $(CURDIR)/debian/tmp

export DEB_BUILD_MAINT_OPTIONS=hardening=+all
CFLAGS = `dpkg-buildflags --get CFLAGS`
CFLAGS += -Wall
LDFLAGS_DEF = `dpkg-buildflags --get LDFLAGS`
CPPFLAGS_DEF = `dpkg-buildflags --get CPPFLAGS`

EXTRA_ARGS=
PROFILE_ARGS=
#to enable debugging: export DEB_BUILD_OPTIONS="debug profiling threadoff"
ifneq (,$(findstring profiling,$(DEB_BUILD_OPTIONS)))
	PROFILE_ARGS= --with-gprof
endif
ifneq (,$(findstring debug,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0 -ggdb -rdynamic -D_GNU_SOURCE -MD -MP -D TECH_PREVIEW
	LDFLAGS+=-Wl,--no-undefined -Wl,--as-needed
	EXTRA_ARGS = --with-backtrace
	ifneq (,$(findstring event,$(DEB_BUILD_OPTIONS)))
		EXTRA_ARGS = --with-backtrace
	endif
else
	LDFLAGS+=-Wl,--no-undefined -Wl,--as-needed
	CFLAGS += -O2
endif

ifneq (,$(findstring backtrace,$(DEB_BUILD_OPTIONS)))
	CFLAGS+= -rdynamic -D_GNU_SOURCE -MD -MP
	LDFLAGS+=-Wl,--no-undefined -Wl,--as-needed
	EXTRA_ARGS += --with-backtrace
endif

ifneq (,$(findstring threadoff,$(DEB_BUILD_OPTIONS)))
	THREAD_ARGS=--without-threaded-client
	CFLAGS += -D WITH_THREADLOG
else
	THREAD_ARGS=
endif

ifneq (,$(findstring iodbg,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -D BIGBAD_IODBG
endif

ifneq (,$(findstring gcov,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -fprofile-arcs -ftest-coverage
	LDFLAGS += -fprofile-arcs -ftest-coverage
endif

override_dh_auto_configure:
	dh_testdir

	CFLAGS="$(CFLAGS)" CPPFLAGS="$(CPPFLAGS_DEF)" LDFLAGS="$(LDFLAGS_DEF)" ./configure \
		--prefix=/var/lib/citadel \
		--with-datadir=/var/lib/citadel \
		--with-helpdir=/usr/share/citadel-server \
		--with-staticdatadir=/etc/citadel \
		--with-spooldir=/var/spool/citadel \
		--with-sysconfdir=/etc/citadel \
		--with-rundir=/var/run/citadel \
		--with-docdir=/usr/share/doc/citadel-doc/ \
		--with-ssldir=/etc/ssl/citadel/ \
		--with-utility-bindir=/usr/lib/citadel-server/ \
		--with-autosysconfdir=/var/lib/citadel/data/ \
		--with-pam \
		--with-db \
		--with-zlib \
		--with-ldap \
		--with-libical \
		--with-libsieve \
		--enable-debug $(EXTRA_ARGS) $(PROFILE_ARGS) $(THREAD_ARGS)

override_dh_auto_clean:
	dh_auto_clean

	-rm -f build-arch-stamp build-indep-stamp configure-stamp
	-rm -f config.status config.log po/citadel-setup/Makefile

override_dh_auto_install:
	dh_testdir
	dh_testroot
	dh_prep -i
	dh_installdirs -i

	$(MAKE) DESTDIR=$(DEB_DESTDIR) install-doc-new install-exec-new install-data-new

override_dh_installinit:
	dh_installinit --name=citadel --restart-after-upgrade

override_dh_strip:
	dh_strip

%:
	dh $@
